import datetime
import logging
import logging.handlers
import time
import os

import aces

LOG_PATH = u'./data/monitoring'
LOG_CONTENT_PATH = os.path.join(LOG_PATH, u'content_log')
LOG_SYNC_FILE_PATTERN = u'content_synchro_{date}_{id}.log'
# Datetime formating for Sync file pattern
LOG_SYNC_FILE_PATTERN_DATE = u'%Y-%m-%d'
LOG_SYNC_FORMAT = u'%(asctime)s %(name)s %(message)s'
LOG_CONTENT_FILE_PATTERN = u'{content_name}.log'
LOG_CONTENT_FORMAT = LOG_SYNC_FORMAT

LOG_SYNC_INFO_SYNC_START = u"Start Sync Process {id}"
LOG_SYNC_INFO_AUTH = u"Authentication {st}"
LOG_SYNC_INFO_GET_CATALOG = u"Get Catalog {st}"
LOG_SYNC_INFO_DL_START = u"Start Download {st}"
LOG_SYNC_INFO_CATALOG_NB_TITLES = u"Catalog Titles: {type_numbers}"
LOG_SYNC_INFO_CATALOG_SIZE = u"Catalog Total Size: {size}KB"
LOG_SYNC_INFO_CATALOG_TITLE = u"    {type} {title}: {size}KB"
LOG_SYNC_INFO_CONTENT_START = u"'{title}' Start Download {size}KB"
LOG_SYNC_INFO_CONTENT_END = u"'{title}' End Download {st} - {dl_size}KB/{size}KB"
LOG_SYNC_INFO_CONTENT_ERROR = u"'{title}' Download Error {error_code} - {dl_size}KB/{size}KB"
LOG_SYNC_INFO_SYNC_END_SUMMARY = u"End Sync Process {id} - {st} - Donwloaded: {dl_title_nb}/{title_nb} titles - {dl_size}KB/{size}KB"
LOG_SYNC_INFO_SYNC_END_CONTENT = u"    {type} {title} {st} {dl_size}KB/{size}KB"

LOG_STATUS_OK = u'OK'
LOG_STATUS_NOK = u'NOK'
LOG_STATUS_ERR_CODE = u'Error:{code}'


def log_status(code):
    if code is None:
        return LOG_STATUS_OK
    if code == 0:
        return LOG_STATUS_NOK
    return LOG_STATUS_ERR_CODE.format(code=code)


class Logger(logging.Logger):
    """ Business generic logger
    """

    class UTCFormatter(logging.Formatter):
        converter = time.gmtime

    def __init__(self, name, path, level=0):
        logging.Logger.__init__(self, name, level)
        self._path = path
        self._config()

    def _config(self):
        """
        Configure the logger
        """
        formatter = Logger.UTCFormatter(LOG_SYNC_FORMAT)

        # Limit size to 10Mo per file, 10 files
        file_handler = logging.handlers.RotatingFileHandler(
            self._path, 'a', 10000000, 10)
        file_handler.setFormatter(formatter)
        self.addHandler(file_handler)

        # Create the console handler
        steam_handler = logging.StreamHandler()
        steam_handler.setFormatter(formatter)
        self.addHandler(steam_handler)


def utc_datetime():
    return datetime.datetime.utcnow()


def sync_log_path(sync_id):
    utc_dt = utc_datetime()
    date_str = utc_dt.strftime(LOG_SYNC_FILE_PATTERN_DATE)
    log_name = LOG_SYNC_FILE_PATTERN.format(date=date_str, id=sync_id)
    return os.path.join(LOG_PATH, log_name)


class SyncLogger(Logger):
    """ Business Logger for Sync operation
    """

    def __init__(self, sync_id, level=0):
        self.sync_id = sync_id
        log_name = '{id}'.format(id=self.sync_id)
        super(
            self.__class__,
            self).__init__(
            log_name,
            sync_log_path(
                self.sync_id),
            level)
        self._contents = {}

    def content_start(self, content):
        """ Log the content sync start """
        title = content['title']
        size = aces.size_to_kb(content['media_size'])
        self.info(LOG_SYNC_INFO_CONTENT_START.format(title=title, size=size))

    def content_end(self, content, error_code=None):
        """ Log the content sync end
        error_code: None if OK, else the error code as int"""
        uuid = content['uuid']
        title = content['title']
        size = int(aces.size_to_kb(content['media_size']))

        self.info(LOG_SYNC_INFO_CONTENT_END.format(
            title=title, size=size, dl_size=size,
            st=log_status(error_code)))

        # Store the state for summary() logging
        self._contents[uuid] = SyncStatus(uuid, error_code, size)

    def summary(self, catalog):
        """ Log the content sync end """
        cat = aces.Catalog(catalog)
        size = cat.size()
        length = cat.len()
        self.info(LOG_SYNC_INFO_SYNC_END_SUMMARY.format(
            id=self.sync_id, st=self._catalog_status_log(),
            dl_title_nb=length, title_nb=length,
            size=size, dl_size=size))
        self._catalog_content_end(cat)

    def catalog(self, catalog):
        """ Log the catalog content """
        cat = aces.Catalog(catalog)
        content_types = cat.content_types()
        type_numbers = " ".join('{}:{}'.format(k, v)
                                for k, v in content_types.items())
        self.info(
            LOG_SYNC_INFO_CATALOG_NB_TITLES.format(
                type_numbers=type_numbers))
        self.info(LOG_SYNC_INFO_CATALOG_SIZE.format(size=cat.size()))
        self._catalog_content(cat)

    def _catalog_content(self, catalog):
        """ Log the catalog content for each content"""
        for content in catalog:
            self.info(LOG_SYNC_INFO_CATALOG_TITLE.format(
                type=content['category'][0],
                title=content['title'],
                size=content['media_size']))

    def _catalog_content_end(self, catalog):
        """ Log the catalog content for each content"""
        for content in catalog:
            # Retrieve loggged content state
            status = self._contents[content['uuid']]
            log_status_str = log_status(status.state)
            self.info(LOG_SYNC_INFO_SYNC_END_CONTENT.format(
                type=content['category'][0],
                title=content['title'],
                st=log_status_str,
                dl_size=status.tx_size,
                size=content['media_size']))

    def _catalog_status_log(self):
        """ Log the catalog content for each content"""
        states = set(content.state for content in self._contents.itervalues())
        # If there is no error_code in states:
        # All if OK
        state = states.pop()
        if state is None:
            return LOG_STATUS_OK
        return LOG_STATUS_NOK


class ContentInfo(object):
    """ Content Info
    """

    def __init__(self, uuid, title, size):
        self.uuid = uuid
        self.title = title
        self.size = size


class SyncStatus(object):
    """ Sync Status
    """

    def __init__(self, uuid, state, tx_size=0):
        self.uuid = uuid
        self.state = state
        self.tx_size = tx_size
