boto3==1.2.4
botocore==1.3.29
Django==1.9.4
django-http-proxy==0.4.3
gunicorn==19.4.5
ipdb
ipython
requests==2.9.1
simplejson
ftputil

./lib/whitenoise
