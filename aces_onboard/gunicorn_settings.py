import multiprocessing, os

bind = "0.0.0.0:%s" % (os.environ['SERVER_PORT'],)
workers = multiprocessing.cpu_count() * 2 + 1
daemon=False

accesslog = '-'
errorlog = '-'
loglevel = 'debug'
