"""aces_onboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import RedirectView

from httpproxy.views import HttpProxy

import importlib
import os

DJANGO_SETTINGS_MODULE=os.environ['DJANGO_SETTINGS_MODULE']
settings = importlib.import_module(DJANGO_SETTINGS_MODULE)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^(?P<url>.*)$', HttpProxy.as_view(base_url=settings.ACES_BASE_URI), name='httpproxy')
]
