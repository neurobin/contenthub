import logging, os, simplejson

from django.http import HttpResponse
from django.shortcuts import render

from httpproxy.views import HttpProxy

import lib.utils
import peplink.settings



def captive(request):

    _PHP_REQUEST = dict(request.session)
    _PHP_REQUEST.update(request.GET.dict())
    _PHP_REQUEST.update(request.POST.dict())

    tnc_text = "Service Terms and Conditions\n\n" \
             + "This free WiFi service (\"Service\") is provided by this company (\"Company\") to its customers. Please read below Service Terms and Conditions. To use the Service, users must accept these Service Terms and Conditions.\n\n" \
             + "    1. The Service allows the user to access the Internet via the Wi-Fi network provided by the Company by using the user's Wi-Fi-enabled device. In order to use the Service, the user must use a Wi-Fi -enabled device and related software. It is the user's responsibility to ensure that the user's device works with the Service.\n\n" \
             + "    2. The Company may from time to time modify or enhance or suspend the Service.\n\n" \
             + "    3. The user acknowledges and consents that:\n" \
             + "        (a) The Service has to be operated properly in accordance with the recommended practice, and with the appropriate hardware and software installed;\n\n" \
             + "        (b) The provisioning of the Service may reveal location-specific data, usage and retention of which are subject to the local standard privacy policy and jurisdiction;\n\n" \
             + "        (c) Every user is entitled to 20 continuous minutes free WiFi service every day at the Company's designated location(s). If the connection is disconnected within the 20 minutes due to any reason, the user cannot use the Service again on the same day;\n\n" \
             + "        (d) The Company excludes all liability or responsibility for any cost, claim, damage or loss to the user or to any third party whether direct or indirect of any kind including revenue, loss or profits or any consequential loss in contract, tort, under any statute or otherwise (including negligence) arising out of or in any way related to the Service (including, but not limited to, any loss to the user arising from a suspension of the Service or Wi-Fi disconnection or degrade of Service quality); and\n\n" \
             + "        (e) The Company will not be liable to the user or any other person for any loss or damage resulting from a delay or failure to perform these Terms and Conditions in whole or in part where such delay or failure is due to causes beyond the Company's reasonable control, or which is not occasioned by its fault or negligence, including acts or omissions of third parties (including telecommunications network operators, Information Service content providers and equipment suppliers), shortage of components, war, the threat of imminent war, riots or other acts of civil disobedience, insurrection, acts of God, restraints imposed by governments or any other supranational legal authority, industrial or trade disputes, fires, explosions, storms, floods, lightening, earthquakes and other natural calamities.\n\n" \
             + "    4. The user's use of the Service is subject to the coverage and connectivity conditions of the Service network and the Company makes no guarantee regarding the service performance and availability of the Service network. The Company hereby expressly reserves the right to cease the provisioning of the Service in the event the same is being substantially affected by reasons beyond the control of the Company.\n\n"

    # we force form_action to http://sita.aero:8008/portal.cgi rather than https://sita.aero:8000/portal.cgi
    # to avoid HTTPS and "Bad SSL certificate" issue
    form_action = "http://sita.aero:8008/portal.cgi"
    # try:
    #     form_action = _PHP_REQUEST['form_action']
    # except KeyError, e:
    #     pass

    page_param = {}
    # /*
    #  * Attributes included in redirection:
    #  * display_mode	- The display mode, possible values are "agree" and "login".
    #  * ip		- The IP address of the client.
    #  * name		- The name of the device.
    #  * sn		- The serial number of the device.
    #  * host_mac	- The MAC address of the device.
    #  * host_ip	- The IP address of the device.
    #  * time		- The current time of the device.
    #  * message	- The message field.
    #  * auth_msg	- The authenticate message.
    #  * type		- The access type.
    #  * status      	- The access status.
    #  */
    try:
        page_param["display_mode"] = _PHP_REQUEST['display_mode']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["client_ip"] = _PHP_REQUEST['ip']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["host_ip"] = _PHP_REQUEST['host_ip']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["message"] = _PHP_REQUEST['message']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["auth_msg"] = _PHP_REQUEST['auth_msg']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["access_type"] = _PHP_REQUEST['type']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["access_status"] = _PHP_REQUEST['status']
    except KeyError, e:
        logging.exception(e)
    # /*
    #  * form_action	- The URL for submitting form.
    #  * Default URL, https://sita.aero:8000/portal.cgi
    #        * will be used when form_action is null
    #  */
    page_param["form_action"] = form_action
    # /*
    #  * Auth code:
    #  * 200 - Login Success
    #  * 401 - Invalid Username/Password
    #  * 402 - Access Quota Reached
    #  * 500 - Internal server Error
    #  */
    try:
        page_param["auth_code"] = _PHP_REQUEST['auth_code']
    except KeyError, e:
        logging.exception(e)
    # /*
    #  * Form attributes:
    #  * username - The username for authentication. If "Open Access" mode
    #  *            is used, this is optional.
    #  * password - The password for authentication. If "Open Access" mode
    #  *            is used, this is optional.
    #  * command  - To indicate the submit mode, possible values are "popup",
    #  *            "logout".
    #  * orig_url - The URL to be redirected to after authentication success.
    # */
    try:
        page_param["command"] = _PHP_REQUEST['command']
    except KeyError, e:
        logging.exception(e)
    try:
        page_param["orig_url"] = _PHP_REQUEST['orig_url']
    except KeyError, e:
        logging.exception(e)
    # /*
    #  * cur_page_url - URL of current page:
    #  */
    page_param["cur_page_url"] = None # curPageURL()
    # /*
    #  * Control successful login action:
    #  * success_page_enable	- true: redirect to success page
    #  *			- false: redirect orig_url
    #  */
    page_param["success_page_enable"] = True
    # /*
    #  * Display content:
    #  */
    page_param["content"] = {
        # /*
        #  * header_msg - Header text
        #  * footer_msg - Footer text
        #  */
        "header_text": "Media On Move",
        "footer_text": "Powered by SITA",
        # /*
        #  * tnc_text - Text of Terms and Conditions
        #  * tnc_title - Title for Terms and Conditions
        #  * tnc_prompt_prefix - Prefix of prompt for confirm Terms and Conditions
        #  * tnc_prompt_suffix - Suffix of prompt for confirm Terms and Conditions
        #  * tnc_accept_warn - Warning for accept Terms and Conditions
        #  */
#        "tnc_text": tnc_text,
        "tnc_title": "Terms and Conditions",
        "tnc_prompt_prefix": "I have read and agree to the ",
        "tnc_prompt_suffix": "",
        "tnc_accept_warn": "You must accept the terms and conditions before you can proceed",
        # /*
        #  * login_button - Login button
        #  * connect_button - Connect button
        #  * continue_button - Continue browsing button
        #  * back_login_button - Back to login page button(in "Terms and Conditions" page)
        #  */
        "login_button": "Login",
        "connect_button": "Discover the service ...",
        "continue_button": "Redirecting... Please Wait",
        "back_login_button": "Back to Login",
        # /*
        #  * login_fail_msg - login fail message
        #  * welcome_msg - Welcome message
        #  */
        "login_fail_msg": "fail_msg",
        "welcome_msg": "Immerse yourself in a unique experience of media discovery",
        # /*
        #  * username_title - Title of input user field
        #  * username_placeholder - Place holder of input user field
        #  * password_title - Title of input password field
        #  * password_placeholder - Place holder of input password field
        #  */
        "username_title": "Username",
        "username_placeholder": "input your username...",
        "password_title": "Password",
        "password_placeholder": "input your password...",
        # /*
        #  * background_img - Path of background image
        #  * logo - Path of logo image
        #  */
        "background_img": "/static/img/default_bg.jpg",
        "logo": "/static/img/logo.png"
    }
    # /*
    #  * Style:
    #  * display - true: show element
    #  *           false: hide element
    #  * background_color - Value of background color
    #  * text_color - Value of text color
    #  */
    page_param["style"] = {
        "header": {
            "display": True,
            "background_color": "#333333",
            "text_color": "#C3C3C3"
        },
        "footer": {
            "display": True,
            "background_color": "#333333",
            "text_color": "#C3C3C3"
        },
        "logo": {
            "display": True
        },
        "main_content": {
            "background_color": "#F7F6F3",
            "text_color": "#555555"
        },
        "tnc_text": {
            "display": False,
            "background_color": "#D8DFEA",
            "text_color": "#3A3935"
        },
        "tnc_accept_warn": {
            "background_color": "#F2DEDE",
            "text_color": "#B94A48"
        },
        "fail_message": {
            "background_color": "#F2DEDE",
            "text_color": "#B94A48"
        },
        "message": {
            "text_color": "#888800"
        },
        "input_placeholder": {
            "text_color": "#555555"
        },
        "submit_button": {
            "background_color": "#428BCA",
            "text_color": "#FFFFFF"
        },
        "back_button": {
            "background_color": "#5DA423",
            "text_color": "#FFFFFF"
        }
    }

    return render(request, 'captive.html', { "page_param_json": simplejson.dumps(page_param) } )



class RegisterView(HttpProxy):

    def dispatch(self, request, url, *args, **kwargs):

        ip = request.META['REMOTE_ADDR']
        if request.META.has_key('HTTP_X_FORWARDED_FOR'):
            ip = request.META['HTTP_X_FORWARDED_FOR']

        mac = None

        try:
            mac = lib.utils.arp_table()[ip]
        except KeyError, e:
            if ip == '127.0.0.1':
                mac = '127.0.0.1'
            else:
                logging.exception(e)
                mac = 'unknown'

        request.session['MAC_ADDR'] = mac

        json_body = simplejson.loads(request.body)
        json_body['appid'] = peplink.settings.ACES_APPID
        json_body['username'] = peplink.settings.ACES_USERNAME
        json_body['password'] = peplink.settings.ACES_PASSWORD
        request._body = simplejson.dumps(json_body)

        response = super(RegisterView, self).dispatch(request, url, *args, **kwargs)
        return response



class CatalogView(HttpProxy):

    def dispatch(self, request, url, *args, **kwargs):

        if request.path.endswith('undefined/'):
            request.path = request.path.replace('undefined', peplink.settings.ACES_CATALOG_UUID)
            url = url.replace('undefined', peplink.settings.ACES_CATALOG_UUID)
        elif not request.path.endswith(peplink.settings.ACES_CATALOG_UUID + '/'):
            request.path += peplink.settings.ACES_CATALOG_UUID + '/'
            url += peplink.settings.ACES_CATALOG_UUID + '/'

        try:
            catalog = None
            with open(os.path.join(peplink.settings.BASE_DIR, '../static/catalog.json'), 'r') as f:
                catalog = f.read()

            response = HttpResponse(catalog)
            response['Content-Type'] = 'application/json'
            return response
        except Exception, e:
            logging.exception(e)
            return super(CatalogView, self).dispatch(request, url, *args, **kwargs)



class DecryptionView(HttpProxy):

    def dispatch(self, request, url, *args, **kwargs):

        if request.path.endswith('undefined/'):
            request.path = request.path.replace('undefined', peplink.settings.ACES_CATALOG_UUID)
            url = url.replace('undefined', peplink.settings.ACES_CATALOG_UUID)
        elif not request.path.endswith(peplink.settings.ACES_CATALOG_UUID + '/'):
            request.path += peplink.settings.ACES_CATALOG_UUID + '/'
            url += peplink.settings.ACES_CATALOG_UUID + '/'

        return super(DecryptionView, self).dispatch(request, url, *args, **kwargs)



class StatisticsView(HttpProxy):

    def dispatch(self, request, url, *args, **kwargs):

        if request.path.endswith('undefined/'):
            request.path = request.path.replace('undefined', peplink.settings.ACES_CATALOG_UUID)
            url = url.replace('undefined', peplink.settings.ACES_CATALOG_UUID)
        elif not request.path.endswith(peplink.settings.ACES_CATALOG_UUID + '/'):
            request.path += peplink.settings.ACES_CATALOG_UUID + '/'
            url += peplink.settings.ACES_CATALOG_UUID + '/'

        json_body = simplejson.loads(request.body)
        json_body['appid'] = peplink.settings.ACES_APPID
        try:
            json_body['mac_address'] = request.session['MAC_ADDR']
        except KeyError, e:
            logging.exception(e)
            json_body['mac_address'] = 'unknown'
        request._body = simplejson.dumps(json_body)

        return super(StatisticsView, self).dispatch(request, url, *args, **kwargs)



class SatisfactionView(HttpProxy):

    def dispatch(self, request, url, *args, **kwargs):

        json_body = simplejson.loads(request.body)
        satisfaction_json = []
        try:
            with open(os.path.join(peplink.settings.BASE_DIR, '../data/satisfaction.json')) as f:
                satisfaction_json = simplejson.loads(f.read())
        except Exception, e:
            logging.exception(e)
        satisfaction_json.append(json_body)
        try:
            with open(os.path.join(peplink.settings.BASE_DIR, '../data/satisfaction.json'), 'w') as f:
                f.write(simplejson.dumps(satisfaction_json))
        except Exception, e:
            logging.exception(e)

        response = HttpResponse('{ "status": "ok" }')
        response['Content-Type'] = 'application/json'
        return response

class SurveyView(HttpProxy):

    def dispatch(self, request, url, *args, **kwargs):

        responses_stack_path = os.path.join(peplink.settings.BASE_DIR, '../data/survey.json')

        json_body = simplejson.loads(request.body)
        survey_json = []
        try:
            with open(responses_stack_path, 'r') as f:
                survey_json = simplejson.loads(f.read())
        except Exception, e:
            logging.exception(e)
        survey_json.append(json_body)
        try:
            with open(responses_stack_path, 'w') as f:
                f.write(simplejson.dumps(survey_json))
        except Exception, e:
            logging.exception(e)

        response = HttpResponse('{ "status": "ok" }')
        response['Content-Type'] = 'application/json'
        return response
