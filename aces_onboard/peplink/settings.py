from aces_onboard.settings import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [ '*' ]

INSTALLED_APPS.append('peplink')

MIDDLEWARE_CLASSES.remove('django.middleware.csrf.CsrfViewMiddleware')

ROOT_URLCONF = 'peplink.urls'

STATICFILES_DIRS += [ os.path.join(BASE_DIR, 'peplink', 'static'),
                      os.path.join(BASE_DIR, 'peplink', 'captive'),
                      os.path.join(BASE_DIR, '..', 'data', 'sync') ]

from config import *
